import { calculateBonuses } from "./bonus-system.js";

describe("Bonus-system tests", () => {
  const precision = 4;

  test("Standard bonus", () => {
    const program = "Standard";
    expect(calculateBonuses(program, 1000)).toBeCloseTo(0.05, precision);
    expect(calculateBonuses(program, 10000)).toBeCloseTo(0.075, precision);
    expect(calculateBonuses(program, 30000)).toBeCloseTo(0.075, precision);
    expect(calculateBonuses(program, 50000)).toBeCloseTo(0.1, precision);
    expect(calculateBonuses(program, 80000)).toBeCloseTo(0.1, precision);
    expect(calculateBonuses(program, 100000)).toBeCloseTo(0.125, precision);
    expect(calculateBonuses(program, 200000)).toBeCloseTo(0.125, precision);
  });

  test("Premium bonus", () => {
    const program = "Premium";
    expect(calculateBonuses(program, 1000)).toBeCloseTo(0.1, precision);
    expect(calculateBonuses(program, 10000)).toBeCloseTo(0.15, precision);
    expect(calculateBonuses(program, 30000)).toBeCloseTo(0.15, precision);
    expect(calculateBonuses(program, 50000)).toBeCloseTo(0.2, precision);
    expect(calculateBonuses(program, 80000)).toBeCloseTo(0.2, precision);
    expect(calculateBonuses(program, 100000)).toBeCloseTo(0.25, precision);
    expect(calculateBonuses(program, 200000)).toBeCloseTo(0.25, precision);
  });

  test("Diamond bonus", () => {
    const program = "Diamond";
    expect(calculateBonuses(program, 1000)).toBeCloseTo(0.2, precision);
    expect(calculateBonuses(program, 10000)).toBeCloseTo(0.3, precision);
    expect(calculateBonuses(program, 30000)).toBeCloseTo(0.3, precision);
    expect(calculateBonuses(program, 50000)).toBeCloseTo(0.4, precision);
    expect(calculateBonuses(program, 80000)).toBeCloseTo(0.4, precision);
    expect(calculateBonuses(program, 100000)).toBeCloseTo(0.5, precision);
    expect(calculateBonuses(program, 200000)).toBeCloseTo(0.5, precision);
  });

  test("Invalid program", () => {
    const program = "Other";
    expect(calculateBonuses(program, 1000)).toBeCloseTo(0, precision);
    expect(calculateBonuses(program, 10000)).toBeCloseTo(0, precision);
    expect(calculateBonuses(program, 30000)).toBeCloseTo(0, precision);
    expect(calculateBonuses(program, 50000)).toBeCloseTo(0, precision);
    expect(calculateBonuses(program, 80000)).toBeCloseTo(0, precision);
    expect(calculateBonuses(program, 100000)).toBeCloseTo(0, precision);
    expect(calculateBonuses(program, 200000)).toBeCloseTo(0, precision);
  });

});